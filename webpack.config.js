const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/web/app.js',
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: 'app.bundle.js',
    },
    resolveLoader: {
        modules: [
            'node_modules',
        ],
    },
    mode: 'development',
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/web/app.html',
        }),
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
        ],
    },
};
