#!/usr/bin/env node

const path = require('path');

const sourceUrl = 'https://data.bathhacked.org/resource/q3r9-27v7.csv';
const dataPath = path.resolve('./data/data.csv');

const https = require('https');
const fs = require('fs');

if (fs.existsSync(dataPath))
{
    fs.unlinkSync(dataPath);
}

const dataStream = fs.createWriteStream(dataPath);

dataStream.on('open', () =>
{
    https.get(sourceUrl, (response) =>
    {
        response.setEncoding('utf8');

        response.on('data', (data) =>
        {
            dataStream.write(data);
            process.stdout.write('.');
        });

        response.on('end', () => dataStream.end());
    });
});

process.on('close', () =>
{
    process.stdout.write(`Data has been downloaded to: ${dataPath}`);
});
