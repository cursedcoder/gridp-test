const factory = require('../meter/factory');

const smartMeters = [];

function generateId()
{
    return Math.random()
        .toString(36)
        .substr(2, 9);
}

async function addSmartMeter(socket, intervalMs)
{
    const meterDay = await factory();
    const id = generateId();

    const interval = setInterval(() =>
    {
        const { time, interpolated } = meterDay.nextTick();

        socket.emit(id, {
            time,
            interpolated: interpolated.toFixed(2),
        });
    }, intervalMs);

    const meter = {
        id,
        socket,
        interval,
        meterDay,
    };

    socket.emit('subscribe.request', { id });

    smartMeters.push(meter);

    return meter;
}

function shutdownBy(fn)
{
    smartMeters.filter(fn)
        .forEach((item) =>
        {
            clearInterval(item.interval);
            smartMeters.splice(smartMeters.indexOf(item));
        });
}

function removeSmartMeter(id)
{
    shutdownBy((item) => item.id === id);
}

function flushBySocket(socket)
{
    shutdownBy((item) => item.socket === socket);
}

module.exports = { addSmartMeter, removeSmartMeter, flushBySocket };
