const express = require('express');

const app = express();

const http = require('http')
    .Server(app);

const io = require('socket.io')(http);

const path = require('path');

app.use(express.static(path.resolve(__dirname, '../../dist')));

app.get('/', (req, res) =>
{
    res.sendFile(path.resolve(__dirname, '../../dist/index.html'));
});

const pool = require('./pool');

io.on('connection', (socket) =>
{
    console.log('a user connected, adding a default interval');

    pool.addSmartMeter(socket, 1000);

    socket.on('add', (interval) =>
    {
        console.log('adding interval: ' + interval);
        pool.addSmartMeter(socket, interval);
    });

    socket.on('disconnect', () =>
    {
        console.log('used disconnected, flushing his meters');
        pool.flushBySocket(socket);
    });
});

http.listen(3000, () =>
{
    console.log('listening on *:3000');
});
