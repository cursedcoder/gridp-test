import Chart from 'chart.js';

const color = Chart.helpers.color;

const chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)',
};

function randomProperty(obj)
{
    const keys = Object.keys(obj);

    return obj[keys[keys.length * Math.random() << 0]];
}

export function generateBackgroundColor()
{
    return color(randomProperty(chartColors))
        .alpha(0.5)
        .rgbString();
}

export function generateBorderColor()
{
    return randomProperty(chartColors);
}

export function generateConfig()
{
    return {
        type: 'line',
        data: {
            labels: [],
            datasets: [],
        },
        options: {
            animation: {},
            responsive: true,
            title: {
                text: 'School Building Power Consumption',
            },
            scales: {
                xAxes: [{
                    type: 'time',
                    time: {
                        tooltipFormat: 'll HH:mm',
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'DateTime',
                    },
                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'kWh/1min',
                        beginAtZero: true,
                    },
                }],
            },
        },
    };
}
