import * as ChartJs from 'chart.js';
import { generateBackgroundColor, generateBorderColor, generateConfig } from './generator';

export default class Chart
{
    constructor(limit, onLoad)
    {
        if (!limit)
        {
            this.limit = 50;
        }
        else
        {
            this.limit = limit;
        }

        this.onLoad = onLoad;

        this.chartjs = null;
        this.callbacks = [];
    }

    initialize()
    {
        const context = document.getElementById('chart')
            .getContext('2d');

        if (!context)
        {
            console.error('Cannot find canvas: #chart');

            return;
        }

        const config = generateConfig();

        config.options.animation.onComplete = () =>
        {
            if (this.onLoad)
            {
                this.onLoad(this);
            }

            config.options.animation.onComplete = () =>
            {
                this.callbacks.forEach((cb) => cb());
                this.callbacks = [];
            };
        };

        this.chartjs = new ChartJs(context, config);
        this.chartjs.update();
    }

    addDataSet(name)
    {
        if (!this.chartjs)
        {
            return;
        }

        const dataSet = {
            label: name,
            data: [],
            backgroundColor: generateBackgroundColor(),
            borderColor: generateBorderColor(),
        };

        this.callbacks.push(() =>
        {
            this.chartjs.data.datasets.push(dataSet);
        });

        return dataSet;
    }

    addData(dataSet, date, consumption)
    {
        if (!this.chartjs)
        {
            return;
        }

        if (dataSet.data.length >= this.limit)
        {
            this.chartjs.data.labels.shift();
            dataSet.data.shift();
        }

        dataSet.data.push({
            t: date,
            y: consumption,
        });

        this.chartjs.update();
    }
}
