import './app.css';
import 'chart.js';
import Chart from './chart/chart';
import { establishChartConnection } from './socket/connection';

window.onload = () =>
{
    const chart = new Chart(50, (chart) =>
    {
        const socket = establishChartConnection(chart);

        const add = document.getElementById('add');
        const interval = document.getElementById('interval');
        const intervalVal = interval.options[interval.selectedIndex].value;

        add.addEventListener('click', () =>
        {
            chart.callbacks.push(() => {
                socket.emit('add', intervalVal);
            });
        });
    });

    chart.initialize();
};
