import io from 'socket.io-client';

export function establishChartConnection(chart)
{
    const socket = io('http://localhost:3000');

    socket.on('subscribe.request', (request) =>
    {
        const dataSet = chart.addDataSet(`Device: ${request.id}`);

        socket.on(request.id, (data) =>
        {
            console.log(request.id, data);
            chart.addData(dataSet, new Date(data.time), data.interpolated);
        });
    });

    return socket;
}
