const csv = require('../reader/csv');
const { getLine } = require('../reader/getLines');
const MeterDay = require('./MeterDay');
const path = require('path');

function getRandomInt(min, max)
{
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

async function createRandomMeterDay()
{
    const absolutePath = path.resolve(__dirname, '../../data/data.csv');

    const header = await getLine(absolutePath, 0);
    const randomLine = await getLine(absolutePath, getRandomInt(1, 1000));

    const csvHeader = await csv.csvToArray(header);
    const object = await csv.csvToObject(csvHeader, randomLine);

    const intervals = [];
    const date = new Date();
    const ticksPerInterval = 30;

    date.setHours(0, 0, 0, 0);

    for (const key in object)
    {
        if (typeof key !== 'string')
        {
            continue;
        }

        const property = object[key];

        if (key[0] === '_')
        {
            intervals.push(parseFloat(property));
        }
    }

    return new MeterDay(date, intervals, ticksPerInterval);
}

module.exports = createRandomMeterDay;
