/**
 * MeterDay provides an interface over data collected by Smart Meter.
 */
class MeterDay
{
    /**
     * @constructor
     *
     * @param {Date} date - Date for this MeterDay
     * @param {Array<Number>} intervals - array of kw/h/30min, by design should be equal parts of the day
     * @param {Number} ticksPerInterval - how many parts/ticks we have to interpolate between intervals
     */
    constructor(date, intervals, ticksPerInterval)
    {
        this.date = date;

        this.intervals = intervals;

        this.ticks = ticksPerInterval * (intervals.length - 1);

        this.ticksPerInterval = ticksPerInterval;
    }

    /**
     * @returns {object} - interpolate time and value
     */
    nextTick()
    {
        if (this.ticks === 0)
        {
            return this.getPreviousValue();
        }

        const diff = this.getNextValue() - this.getPreviousValue();
        const diffCoefficient = diff / this.ticksPerInterval;

        const totalTicks = this.ticksPerInterval * (this.intervals.length - 1);
        const ticksSincePreviousValue = (totalTicks - this.ticks) - (this.ticksPerInterval * this.getCurrentIndex());

        const interpolated = this.getPreviousValue() + (ticksSincePreviousValue * diffCoefficient);
        const time = (Number(this.date)) + ((totalTicks - this.ticks) * 60000);

        this.ticks--;

        return { time, interpolated };
    }

    /**
     * @returns {number} - index of current time interval
     */
    getCurrentIndex()
    {
        return this.intervals.length - Math.ceil((this.ticks / this.ticksPerInterval)) - 1;
    }

    /**
     * Left side of time interval
     *
     * @returns {number|undefined} - previous value
     */
    getPreviousValue()
    {
        return this.intervals[this.getCurrentIndex()];
    }

    /**
     * Right side of time interval
     *
     * @returns {number|undefined} - next value
     */
    getNextValue()
    {
        return this.intervals[this.getCurrentIndex() + 1];
    }
}

module.exports = MeterDay;
