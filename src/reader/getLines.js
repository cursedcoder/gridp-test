const readline = require('readline');

const fs = require('fs');

async function getLines(filename, start, length)
{
    if (!length)
    {
        length = 1;
    }

    const lineReader = readline
        .createInterface({
            input: fs
                .createReadStream(filename),
        });

    let lineNumber = 0;
    const result = [];

    lineReader.on('line', (line) =>
    {
        const leftBorder = lineNumber >= start;
        const rightBorder = lineNumber < start + length;

        if (leftBorder && rightBorder)
        {
            result.push(line);

            const isFinalLine = lineNumber === length + start;

            if (isFinalLine)
            {
                lineReader.close();
            }
        }

        lineNumber++;
    });

    return new Promise((resolve, reject) =>
    {
        lineReader.on('error', reject);
        lineReader.on('close', resolve.bind(resolve, result));
    });
}

async function getLine(filename, line)
{
    return getLines(filename, line, 1)
        .then((array) => array.pop());
}

module.exports = { getLines, getLine };
