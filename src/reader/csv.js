const csv = require('csv');

function makeObject(keys, vals)
{
    const object = {};

    keys.forEach((key, i) =>
    {
        object[key] = vals[i];
    });

    return object;
}

async function csvToArray(line)
{
    return new Promise((resolve, reject) =>
    {
        csv.parse(line, (err, data) =>
        {
            if (err)
            {
                reject(err);

                return;
            }

            resolve(data.pop());
        });
    });
}

async function csvToObject(header, line)
{
    return csvToArray(line)
        .then(makeObject.bind(null, header));
}

module.exports = { csvToArray, csvToObject };
