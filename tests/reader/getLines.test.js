const expect = require('chai').expect;
const getLines = require('../../src/reader/getLines');
const path = require('path');

describe('getLines file reader', () =>
{
    it('gets lines for no args', async () =>
    {
        const lines = await getLines(path.resolve(`${__dirname}/fixture.txt`));

        expect(lines)
            .length(0);
    });

    it('gets lines for position and length', async () =>
    {
        const lines = await getLines(path.resolve(`${__dirname}/fixture.txt`), 0, 1);

        expect(lines)
            .length(1);
        expect(lines[0])
            .eq('header');
    });

    it('gets lines for position and length multiple lines', async () =>
    {
        const lines = await getLines(path.resolve(`${__dirname}/fixture.txt`), 2, 3);

        expect(lines)
            .length(3);
        expect(lines[0])
            .eq('second line');
        expect(lines[1])
            .eq('third line');
        expect(lines[2])
            .eq('final line');
    });
});
