const csv = require('../../src/reader/csv');
const expect = require('chai').expect;

describe('lineToCsv converts csv line to object', () =>
{
    it('converts the line to an object', async () =>
    {
        const headerCsv = '"date","degree_days","school","type"';
        const lineCsv = '"2010-09-01T00:00:00.000","2.4","A","gas"';

        const header = await csv.csvToArray(headerCsv);
        const converted = await csv.csvToObject(header, lineCsv);

        expect(converted)
            .to
            .deep
            .equal({
                date: '2010-09-01T00:00:00.000',
                degree_days: '2.4',
                school: 'A',
                type: 'gas',
            });
    });
});
