const MeterDay = require('../../src/meter/MeterDay');
const expect = require('chai').expect;

describe('MeterDay object interpolates date intervals', () =>
{
    it('has current index', () =>
    {
        const meter = new MeterDay(new Date(), [1, 2, 3], 10);

        expect(meter.getCurrentIndex())
            .to
            .eq(0);
    });

    it('it can retrieve previous and next value', () =>
    {
        const meter = new MeterDay(new Date(), [
            1,
            2,
            3,
            4,
            5,
        ], 10);

        expect(meter.getPreviousValue())
            .is
            .eq(1);
        expect(meter.getNextValue())
            .is
            .eq(2);
    });

    it('changes previous and next value after ticks', () =>
    {
        const meter = new MeterDay(new Date(), [
            1,
            2,
            3,
        ], 1);

        expect(meter.getPreviousValue())
            .is
            .eq(1);
        expect(meter.getNextValue())
            .is
            .eq(2);

        expect(meter.nextTick().interpolated)
            .to
            .eq(1);

        expect(meter.getPreviousValue())
            .is
            .eq(2);
        expect(meter.getNextValue())
            .is
            .eq(3);

        expect(meter.nextTick().interpolated)
            .to
            .eq(2);

        expect(meter.getPreviousValue())
            .is
            .eq(3);
        expect(meter.getNextValue())
            .is
            .eq();
    });

    it('can interpolate ticks', () =>
    {
        const ticksPerInterval = 10;
        const intervals = [
            1,
            2,
            3,
        ];

        const meter = new MeterDay(new Date(), intervals, ticksPerInterval);

        for (let i = 0; i < ticksPerInterval * (intervals.length - 1); i++)
        {
            expect(meter.nextTick()
                .interpolated
                .toFixed(1))
                .to
                .eq((1 + (i * 0.1)).toFixed(1));
        }
    });

    it('should interpolate with negative numbers', () =>
    {
        const ticksPerInterval = 10;
        const intervals = [
            10,
            5,
            25,
        ];

        const meter = new MeterDay(new Date(), intervals, ticksPerInterval);
        const results = [];

        for (let i = 0; i <= ticksPerInterval * (intervals.length - 1); i++)
        {
            results.push(meter.nextTick());
        }

        expect(results.pop())
            .to
            .eq(25);
    });

    it('should interpolate time based on date', () =>
    {
        const ticksPerInterval = 480;
        const intervals = [
            10,
            5,
            25,
        ];

        const date = new Date();

        date.setUTCHours(0, 0, 0, 0);

        const meter = new MeterDay(date, intervals, ticksPerInterval);

        const tick1 = meter.nextTick();
        const date1 = new Date(tick1.time);

        expect(date1.getMinutes())
            .to
            .eq(0);

        const tick2 = meter.nextTick();
        const date2 = new Date(tick2.time);

        expect(date2.getMinutes())
            .to
            .eq(1);
    });
});
