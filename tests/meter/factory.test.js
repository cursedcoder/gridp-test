const factory = require('../../src/meter/factory');
const MeterDay = require('../../src/meter/MeterDay');
const expect = require('chai').expect;

describe('MeterDay factory produces new objects', () =>
{
    it('can create random MeterDay', async () =>
    {
        const meter = await factory();

        expect(meter)
            .to
            .be
            .an
            .instanceOf(MeterDay);

        expect(meter.intervals.length)
            .to
            .eq(48);

        expect(meter.ticks)
            .to
            .eq(1410);
    });
});
