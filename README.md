Grid+ Test
==========

How to run development:

    npm i -g yarn
    yarn
    
download data
    
    yarn assets
    
build frontend and run service

    yarn web:build
    yarn service

also for hot-reload and etc. you can use

    yarn web:dev
